<?php
session_start();
include_once("config.php");
if (!isset($_SESSION["username"]))
{
    echo "<script>alert('silahkan login');</script>";
    echo "<script>location='login.php';</script>";
}
?>

<html>
<head>    
    <title>Data Nikah</title>
    <link rel="stylesheet" type="text/css" href="assets/css/table.css">
</head>

<body>
    <h2>Data Calon Suami dan Istri</h2>
    <table rules="all" class="table1">
    <tr>
        <th>ID User</th>
        <th>Calon Suami</th>
        <th>NIK Calon Suami</th>
        <th>Tempat dan Tanggal Lahir</th>
        <th>Agama</th>
        <th>Pekerjaan</th>
        <th>Calon Istri</th>
        <th>NIK Calon Istri</th>
        <th>Tempat dan Tanggal Lahir</th>
        <th>Agama</th>
        <th>Pekerjaan</th>
        <th>Tempat Tinggal</th>
        <th>Tanggal Nikah</th>
        <th></th>
    </tr>
    <?php
    $result = mysqli_query($mysqli, "SELECT * FROM data_nikah ORDER BY id_user DESC");  
    while($user_data = mysqli_fetch_array($result)) {         
        echo "<tr>";
        echo "<td>".$user_data['id_user']."</td>";
        echo "<td>".$user_data['nama_suami']."</td>";
        echo "<td>".$user_data['nik_suami']."</td>";
        echo "<td>".$user_data['tempat_lahir_suami']."</td>";
        echo "<td>".$user_data['agama_suami']."</td>";
        echo "<td>".$user_data['pekerjaan_suami']."</td>";
        echo "<td>".$user_data['nama_istri']."</td>";
        echo "<td>".$user_data['nik_istri']."</td>";
        echo "<td>".$user_data['tempat_lahir_istri']."</td>";
        echo "<td>".$user_data['agama_istri']."</td>";
        echo "<td>".$user_data['pekerjaan_istri']."</td>";
        echo "<td>".$user_data['tempat_nikah']."</td>";
        echo "<td>".$user_data['tanggal_nikah']."</td>";    
        echo "<td><button href='edit.php?id=$user_data[id_user]'>Edit</button></td></tr>";        
    }
    ?>
    </table>
    <br/><br/>
    <button href="add.php">Tambah Data</button>

    <!-- jika sudah login(ada session pelanggan) -->
            <?php if (isset($_SESSION["username"])): ?>
                <button><a href="logout.php">Logout</a></button>
            <!-- selain itu (belum login=belum ada session pelaggan) -->
            <?php else: ?>
            <a href="login.php">Login</a>
            <?php endif ?>
</body>
</html>