<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Form Pernikahan</title>

        <!-- CSS -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="assets/css/form-elements.css">
        <link rel="stylesheet" href="assets/css/style.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Favicon and touch icons -->
        <link rel="shortcut icon" href="assets/ico/favicon.png">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">

    </head>
    <body>
        <!-- Top menu -->
        <nav class="navbar navbar-inverse navbar-no-bg" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <h3><b><a href="login.php">Login</a></b></h3>
                </div>
            </div>
        </nav>

        <div class="top-content">
            <div class="container">
                
                <div class="row">
                    <div class="col-sm-8 col-sm-offset-2 text">
                        <h1>Website Pendaftaran Nikah</h1>
                        <a href=""></a>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3 form-box">
                        <form role="form" method="post" class="f1" action="">

                            <h3>Form Pernikahan</h3>
                            <div class="f1-steps">
                                <div class="f1-progress">
                                    <div class="f1-progress-line" data-now-value="16.66" data-number-of-steps="3" style="width: 16.66%;"></div>
                                </div>
                                <div class="f1-step active">
                                    <div class="f1-step-icon"><i></i>1</div>
                                    <p>Step 1</p>
                                </div>
                                <div class="f1-step">
                                    <div class="f1-step-icon"><i></i>2</div>
                                    <p>Step 2</p>
                                </div>
                                <div class="f1-step">
                                    <div class="f1-step-icon"><i></i>3</div>
                                    <p>Step 3</p>
                                </div>
                            </div>
                            
                            <fieldset>
                                <h4>Data Calon Suami</h4>
                                <div class="form-group">
                                    <label class="sr-only" for="f1-first-name">Nama Lengkap</label>
                                    <input type="text" name="nama_suami" placeholder="Nama Lengkap..." class="f1-first-name form-control" id="f1-first-name">
                                </div>
                                <div class="form-group">
                                    <label class="sr-only" for="f1-last-name">NIK</label>
                                    <input type="text" name="nik_suami" placeholder="NIK..." class="f1-last-name form-control" id="f1-last-name">
                                </div>
                                <div class="form-group">
                                    <label class="sr-only" for="f1-about-yourself">Tempat dan Tanggal Lahir</label>
                                    <input type="text" name="tempat_lahir_suami" placeholder="Tempat dan Tanggal Lahir..." class="f1-last-name form-control" id="f1-last-name">
                                </div>
                                <div class="form-group">
                                    <label class="sr-only" for="f1-about-yourself">Status Perkawinan</label>
                                    <input type="text" name="umur_suami" placeholder="Status Perkawinan..." class="f1-last-name form-control" id="f1-last-name">
                                </div>
                                <div class="form-group">
                                    <label class="sr-only" for="f1-about-yourself">Agama</label>
                                    <input type="text" name="agama_suami" placeholder="Agama..." class="f1-last-name form-control" id="f1-last-name">
                                </div>
                                <div class="form-group">
                                    <label class="sr-only" for="f1-about-yourself">Pekerjaan</label>
                                    <input type="text" name="pekerjaan_suami" placeholder="Pekerjaan..." class="f1-last-name form-control" id="f1-last-name">
                                </div>
                                   <div class="form-group">
                                    <label class="sr-only" for="f1-about-yourself">Tempat Tinggal</label>
                                    <input type="text" name="tempat_tinggal_suami" placeholder="Tempat Tinggal..." class="f1-last-name form-control" id="f1-last-name">
                                </div>
                                <div class="f1-buttons">
                                    <button type="button" class="btn btn-next">Next</button>
                                </div>
                            </fieldset>

                            <fieldset>
                                <h4>Data Calon Istri</h4>
                                <div class="form-group">
                                    <label class="sr-only" for="f1-email">Nama Lengkap</label>
                                    <input type="text" name="nama_istri" placeholder="Nama Lengkap..." class="f1-email form-control" id="f1-email">
                                </div>
                                <div class="form-group">
                                    <label class="sr-only" for="f1-password">NIK</label>
                                    <input type="text" name="nik_istri" placeholder="NIK..." class="f1-password form-control" id="f1-password">
                                </div>
                                <div class="form-group">
                                    <label class="sr-only" for="f1-about-yourself">Tempat dan Tanggal Lahir</label>
                                    <input type="text" name="tempat_lahir_istri" placeholder="Tempat dan Tanggal Lahir..." class="f1-last-name form-control" id="f1-last-name">
                                </div>
                                <div class="form-group">
                                    <label class="sr-only" for="f1-about-yourself">Status Perkawinan</label>
                                    <input type="text" name="umur_istri" placeholder="Status Perkawinan..." class="f1-last-name form-control" id="f1-last-name">
                                </div>
                                <div class="form-group">
                                    <label class="sr-only" for="f1-about-yourself">Agama</label>
                                    <input type="text" name="agama_istri" placeholder="Agama..." class="f1-last-name form-control" id="f1-last-name">
                                </div>
                                <div class="form-group">
                                    <label class="sr-only" for="f1-about-yourself">Pekerjaan</label>
                                    <input type="text" name="pekerjaan_istri" placeholder="Pekerjaan..." class="f1-last-name form-control" id="f1-last-name">
                                </div>
                                   <div class="form-group">
                                    <label class="sr-only" for="f1-about-yourself">Tempat Tinggal</label>
                                    <input type="text" name="tempat_tinggal_istri" placeholder="Tempat Tinggal..." class="f1-last-name form-control" id="f1-last-name">
                                </div>
                                <div class="f1-buttons">
                                    <button type="button" class="btn btn-previous">Previous</button>
                                    <button type="button" class="btn btn-next">Next</button>
                                </div>
                            </fieldset>

                            <fieldset>
                                <h4>Jadwal Nikah</h4>
                                <div class="form-group">
                                    <label class="sr-only" for="f1-facebook">Tempat Nikah</label>
                                    <input type="text" name="tempat_nikah" placeholder="Tempat Nikah..." class="f1-facebook form-control" id="f1-facebook">
                                </div>
                                <div class="form-group">
                                    <label class="sr-only" for="f1-twitter">Tanggal Nikah</label>
                                    <input type="text" name="tanggal_nikah" placeholder="Tanggal Nikah..." class="f1-twitter form-control" id="f1-twitter">
                                </div>
                                <div class="f1-buttons">
                                    <button type="button" class="btn btn-previous">Previous</button>
                                    <button type="submit" name="Submit" class="btn btn-submit">Submit</button>
                                </div>
                            </fieldset>
                        
                        </form>
                    </div>
                </div>
                    
            </div>
        </div>


        <!-- Javascript -->
        <script src="assets/js/jquery-1.11.1.min.js"></script>
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/js/jquery.backstretch.min.js"></script>
        <script src="assets/js/retina-1.1.0.min.js"></script>
        <script src="assets/js/scripts.js"></script>
        
        <!--[if lt IE 10]>
            <script src="assets/js/placeholder.js"></script>
        <![endif]-->
        <?php

    // Check If form submitted, insert form data into users table.
    if(isset($_POST['Submit'])) {
        $nama_suami = $_POST['nama_suami'];
        $nik_suami = $_POST['nik_suami'];
        $tempat_lahir_suami = $_POST['tempat_lahir_suami'];
        $umur_suami = $_POST['umur_suami'];
        $agama_suami = $_POST['agama_suami'];
        $pekerjaan_suami = $_POST['pekerjaan_suami'];
        $tempat_tinggal_suami = $_POST['tempat_tinggal_suami'];
        $nama_istri = $_POST['nama_istri'];
        $nik_istri = $_POST['nik_istri'];
        $tempat_lahir_istri = $_POST['tempat_lahir_istri'];
        $umur_istri = $_POST['umur_istri'];
        $agama_istri = $_POST['agama_istri'];
        $pekerjaan_istri = $_POST['pekerjaan_istri'];
        $tempat_tinggal_istri = $_POST['tempat_tinggal_istri'];
        $tempat_nikah = $_POST['tempat_nikah'];
        $tanggal_nikah = $_POST['tanggal_nikah'];


        // include database connection file
        include_once("config.php");

        // Insert user data into table
        $result = mysqli_query($mysqli, "INSERT INTO data_nikah(nama_suami,nik_suami,tempat_lahir_suami,umur_suami,agama_suami,pekerjaan_suami,tempat_tinggal_suami,nama_istri,nik_istri,tempat_lahir_istri,umur_istri,agama_istri,pekerjaan_istri,tempat_tinggal_istri,tempat_nikah,tanggal_nikah) VALUES('$nama_suami','$nik_suami','$tempat_lahir_suami','$umur_suami','$agama_suami','$pekerjaan_suami','$tempat_tinggal_suami','$nama_istri','$nik_istri','$tempat_lahir_istri','$umur_istri','$agama_istri','$pekerjaan_istri','$tempat_tinggal_istri','$tempat_nikah','$tanggal_nikah')");

        // Show message when user added
        echo "<b>Selamat, anda berhasil menambahkan data.</b>";
    }
    ?>

    </body>

</html>