-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 20, 2020 at 08:46 PM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `form_nikah`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id_admin` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `nama` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id_admin`, `username`, `password`, `nama`) VALUES
(1, 'admin', 'admin123', 'Administrator');

-- --------------------------------------------------------

--
-- Table structure for table `data_nikah`
--

CREATE TABLE `data_nikah` (
  `id_user` int(11) NOT NULL,
  `nama_suami` varchar(50) NOT NULL,
  `nik_suami` varchar(50) NOT NULL,
  `tempat_lahir_suami` varchar(50) NOT NULL,
  `umur_suami` varchar(25) NOT NULL,
  `agama_suami` varchar(50) NOT NULL,
  `pekerjaan_suami` varchar(50) NOT NULL,
  `tempat_tinggal_suami` varchar(50) NOT NULL,
  `nama_istri` varchar(50) NOT NULL,
  `nik_istri` varchar(50) NOT NULL,
  `tempat_lahir_istri` varchar(50) NOT NULL,
  `umur_istri` varchar(50) NOT NULL,
  `agama_istri` varchar(50) NOT NULL,
  `pekerjaan_istri` varchar(50) NOT NULL,
  `tempat_tinggal_istri` varchar(50) NOT NULL,
  `tempat_nikah` varchar(100) NOT NULL,
  `tanggal_nikah` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data_nikah`
--

INSERT INTO `data_nikah` (`id_user`, `nama_suami`, `nik_suami`, `tempat_lahir_suami`, `umur_suami`, `agama_suami`, `pekerjaan_suami`, `tempat_tinggal_suami`, `nama_istri`, `nik_istri`, `tempat_lahir_istri`, `umur_istri`, `agama_istri`, `pekerjaan_istri`, `tempat_tinggal_istri`, `tempat_nikah`, `tanggal_nikah`) VALUES
(16, 'Yustria Mahendra Akbar', '3518112012', 'Nganjuk, 20 Desember 1998', 'Belum Kawin', 'Islam', 'Web Developer', 'Surabaya', 'Maharani Yulindari', '35181234', 'Kediri, 20 November 1999', 'Belum Kawin', 'Islam', 'Perawat', 'Kediri', 'Kediri', '20/12/2019'),
(17, 'Hariono', '35161880', 'Lamongan, 13 November 1996', 'Belum Kawin', 'Islam', 'Tukang Las', 'Surabaya', 'Sulastri', '32151770', 'Kediri, 14 September 1997', 'Belum Kawin', 'Islam', 'Guru', 'Kediri', 'Kediri', '20/01/2020'),
(20, 'Haryo', '321619920', 'Yogyakarta, 15 Februari 1995', 'Belum Kawin', 'Islam', 'Guru', 'Kediri', 'Indah Permatasari', '3518990', 'Kediri, 13 Januari 1997', 'Belum Menikah', 'Islam', 'Guru', 'Kediri', 'Kediri', '20/12/2019'),
(21, 'Yustria Akbar', '35182012', 'Nganjuk, 20 Desember 1999', 'Belum Menikah', 'Islam', 'Mobile Developer', 'Nganjuk', 'Dian Asih', '3216177', 'Nganjuk, 03 Januari 1998', 'Belum Menikah', 'Islam', 'Perawat', 'Nganjuk', 'Nganjuk', '20/12/2019'),
(22, 'Bima Panji', '35161990', 'Yogyakarta, 12 Desember 1997', 'Belum Kawin', 'Islam', 'Tukang Foto', 'Surabaya', 'Dian Intan', '351812331', 'Kediri, 11 November 1998', 'Belum Kawin', 'Islam', 'Perawat', 'Kediri', 'Kediri', '29/12/2019'),
(24, 'Yustria Akbar', '35182012', 'Nganjuk, 20 Desember 1999', 'Belum Menikah', 'Islam', 'Mobile Developer', 'Nganjuk', 'Dian Asih', '3216177', 'Nganjuk, 03 Januari 1998', 'Belum Menikah', 'Islam', 'Perawat', 'Nganjuk', 'Nganjuk', '20/12/2019');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indexes for table `data_nikah`
--
ALTER TABLE `data_nikah`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id_admin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `data_nikah`
--
ALTER TABLE `data_nikah`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
